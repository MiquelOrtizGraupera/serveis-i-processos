package camises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Ex_Camises {
    public static void main(String[] args) {

        List<Camises> llista = new ArrayList<>();

        Camises primera = new Camises("Home","XL","Vermell");
        Camises segona = new Camises("Nen","S","Blau");
        Camises tercera = new Camises("Dona","M","Blau");
        Camises cuarta = new Camises("Dona","M", "Vermell");
        Camises cinquena = new Camises("Home", "XL","Negre");

        llista = Arrays.asList(primera,segona,tercera,cuarta,cinquena);
        Strategy(llista);
    }

    public static void Strategy(List<Camises> llista){
        Stream<Camises> camisa = llista.stream().filter(x -> x.getTalla().equals("XL"));
        camisa.forEach(System.out::println);

        camisa = llista.stream().filter(x -> x.getColor().equals("Vermell"));
        camisa.forEach(System.out::println);

        camisa = llista.stream().filter(x -> x.getTalla().equals("M")).filter(y -> y.getColor().equals("Blau"));
        camisa.forEach(System.out::println);
    }
}
