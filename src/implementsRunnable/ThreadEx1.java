package implementsRunnable;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class ThreadEx1 implements Runnable{
    public String fitxer;
    public File file;

    public ThreadEx1(String fitxer) {
        this.fitxer = fitxer;
        this.file = new File(fitxer);
    }


    public void crearFitxer(){
        Path home = Path.of(System.getProperty("user.home"));
        Path definitiu = home.resolve("IdeaProjects/serveis-i-processos/src/implementsRunnable");
        Path fitxerDef = definitiu.resolve(fitxer);
        //Creem el fitxer si no existeix
        try{
            if(!Files.exists(fitxerDef)){
                Files.createFile(fitxerDef);
                System.out.println("Fitxer creat amb éxit");
            }else{
                System.out.println("El"+ fitxer +" ja existeix");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        //Introduim dades al fitxer creat numero del 0 al 9
        try{
            OutputStream desdeForaIntroduim =
                    Files.newOutputStream(fitxerDef, StandardOpenOption.TRUNCATE_EXISTING,StandardOpenOption.CREATE);
            PrintStream escriureDades = new PrintStream(desdeForaIntroduim, true);
            for(int i=0; i<10;i++){
                escriureDades.println(i);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        //Llegim les dades del fitxer i les comptem
        try{
            Scanner scanner = new Scanner(fitxerDef);
            int count = 0;
            while(scanner.hasNext()){
               int num = scanner.nextInt();
               count++;
                System.out.println(num + " Del fitxer "+ Thread.currentThread().getName()+" "+count);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        crearFitxer();
    }
}


