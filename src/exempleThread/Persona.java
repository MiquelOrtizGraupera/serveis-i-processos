package exempleThread;

public class Persona implements Runnable{
    private String nom;
    public Persona(String s){
        nom =s;
    }

    @Override
    public void run() {
        System.out.println("\n"+nom+" Està comptant...dins el thread "+
                Thread.currentThread().getName());
        for (int i = 0; i<10;i++){
            System.out.println(nom+" "+i);
        }
    }
}
