package exempleThread;

public class ExemplePersonaRunnable {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Persona("Monste"));
        Thread t2 = new Thread(new Persona("Clara"));
        Thread t3 = new Thread(new Persona("Ferran"));

        t1.start();
        t2.start();
        t3.start();
    }
}