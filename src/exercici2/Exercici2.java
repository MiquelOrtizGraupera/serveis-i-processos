package exercici2;

import java.util.Locale;
import java.util.Scanner;

public class Exercici2 extends Thread{
     long esPrimo;

    public Exercici2(long esPrimo) {
        this.esPrimo = esPrimo;
    }

    public static boolean esEnter(Scanner lector){
        lector = new Scanner(System.in).useLocale(Locale.US);
        long esPrimo = lector.nextLong();
       //El 0,1 y 4 no son primos
        if(esPrimo == 0 || esPrimo == 1 || esPrimo ==4){
            return false;
        }
        for(int x=2; x < esPrimo /2; x++){
            if(esPrimo % x ==0)
                return false;
        }
        return true;
    }

    @Override
    public void run() {
        Scanner lector = new Scanner(System.in);
        esEnter(lector);


    }

    public static void main(String[] args) {

    }
}
