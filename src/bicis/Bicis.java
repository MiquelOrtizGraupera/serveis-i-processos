package bicis;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Bicis extends Thread{
    //nom propietari de la bici
    final String nom;
    //moment que comença el viatge en bici
    final LocalTime inici;
    //distància a recórrer
    final int distancia;
    private long temps;


    public Bicis(String nom, LocalTime inici, int distancia) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public void run() {
        LocalTime fi = LocalTime.now();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        temps = Duration.between(inici,fi).getNano();

//        System.out.println(this.nom+ " ha sortit a les... " + this.inici+ " ha recorregut "+ this.distancia+"km"
//                +" amb aquest temps: "+aux);
    }

    public static void main(String[] args) throws InterruptedException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime inici = LocalTime.parse(LocalTime.now().format(dtf));
        final int distancia = 1000;

        Bicis b1 = new Bicis("Montse", inici, distancia);
        Bicis b2 = new Bicis("Fran", inici, distancia);
        Bicis b3 = new Bicis("Clara", inici, distancia);

        ArrayList<Bicis> llista = new ArrayList<>();
        llista.add(b1);
        llista.add(b2);
        llista.add(b3);

        b1.start();
        b2.start();
        b3.start();
//join imprescindible ja que el metode join evita que passi res avanç no acabin els threads.
        /*b1.join();
        b2.join();
        b3.join();*/
        System.out.println(b1.getTemps());
        System.out.println(b2.getTemps());
        System.out.println(b3.getTemps());
    }

    @Override
    public String toString() {
        return "Bicis{" +
                "nom='" + nom + '\'' +
                ", inici=" + inici +
                ", distancia=" + distancia +
                ", nano=" +
                '}';
    }
}
