package thread;

public class mainSegonaPart {
    public static void main(String[] args) {
        Ex1SegonaPart f1 = new Ex1SegonaPart("futbol.txt");
        Ex1SegonaPart f2 = new Ex1SegonaPart("basquet.txt");
        Ex1SegonaPart f3 = new Ex1SegonaPart("hockey.txt");

        f1.start();
        f2.start();
    }
}
